package com.example.demo.controller;

import com.example.demo.model.Category;
import com.example.demo.model.Product;
import com.example.demo.service.CategoryService;
import com.example.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;


    @RequestMapping("/index")
    public String allProduct(Model model,  @Param("keyword") String keyword ) {
        if (productService.listAll().size() == 0 && categoryService.listAll().size() == 0) {
            Date date = new Date();
            Category c1 = new Category((long) 1, date, "Bread", true, date);
            Category c2 = new Category((long) 2, date, "Cake", true, date);
            Category c3 = new Category((long) 3, date, "Pudding", true, date);
            Category c4 = new Category((long) 4, date, "Baking ingredients", true, date);
            Category c5 = new Category((long) 6, date, "Cupcake", true, date);
            List<Category> categoryList = new ArrayList<Category>();categoryList.add(c1);categoryList.add(c2);categoryList.add(c3);categoryList.add(c4);categoryList.add(c5);

            Product p1 = new Product((long)1, date , "Freshly baked every day !", "bread-9.png", "Baguette", "vn", (float)20, (long)2, true, date, c1);
            Product p2 = new Product((long)2, date, "Full of flavor with chocolaty smooth texture !", "bread-11.png", "Chocolate Pudding", "usa", (float)14, (long)15, true, date, c3);
            Product p3 = new Product((long)13, date, "Healthy for those who are dieting !", "bread-2.jpg", "Brown Bread", "usa", (float)5, (long)15, true, date, c1);
            Product p4 = new Product((long)14, date, "Healthy for those who are dieting !", "strawberrycake.png", "Strawberry Cake", "usa", (float)35, (long)10, true, date, c2);
            List<Product> productList = new ArrayList<Product>(); productList.add(p1);productList.add(p2);productList.add(p3);productList.add(p4);

            categoryService.saveAll(categoryList); productService.saveAll(productList);
        }
        model.addAttribute("products", productService.listAll(keyword));
        model.addAttribute("keyword", keyword);
        //model.addAttribute("pro",new Product("ao 2","1234",1000L));
        return "product/index";
    }

    @GetMapping("/create")
    public String showNewProductPage(Model model) {
        Product product = new Product();
        model.addAttribute("categories", categoryService.listAll());
        model.addAttribute("product", product);

        return "product/create";
    }

    @RequestMapping(value = "/create/save", method = RequestMethod.POST)
    public String saveCreateProduct(@ModelAttribute("product") Product product) {
        Date date = new Date();
        product.setCreated_at(date);
        product.setUpdated_at(date);
        productService.save(product);

        return "redirect:/product/index";
    }

    @RequestMapping(value = "/edit/save/{id}", method = RequestMethod.POST)
    public String saveEditProduct(@PathVariable("id") long id,@ModelAttribute("product") Product product) {

        Date date = new Date();
        product.setCreated_at(productService.get(id).getCreated_at());
        product.setUpdated_at(date);
        productService.save(product);
        return "redirect:/product/index";
    }

    @RequestMapping("/delete/{id}")
    public String deleteProduct(@PathVariable(name = "id") Long id) {
        productService.delete(id);
        return "redirect:/product/index";
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView showEditProductPage(@PathVariable(name = "id") int id) {
        ModelAndView mav = new ModelAndView("/product/edit");
        Product product = productService.get(id);
        mav.addObject("categories", categoryService.listAll());
        mav.addObject("product", product);
        return mav;
    }
}
