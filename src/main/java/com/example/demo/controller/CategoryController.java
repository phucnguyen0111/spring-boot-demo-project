package com.example.demo.controller;

import com.example.demo.model.Category;
import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

@Controller
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping("/index")
    public String allCategory(Model model, @Param("keyword") String keyword) {
        model.addAttribute("categories", categoryService.listAll(keyword));
        model.addAttribute("keyword", keyword);
        return "category/index";
    }

    @GetMapping("/create")
    public String showNewCategoryPage(Model model) {
        Category category = new Category();
        model.addAttribute("category", category);
        return "category/create";
    }

    @RequestMapping(value = "/create/save", method = RequestMethod.POST)
    public String saveCreateCategory(@ModelAttribute("category") Category category) {
        Date date = new Date();
        category.setCreated_at(date);
        category.setUpdated_at(date);
        categoryService.save(category);

        return "redirect:/category/index";
    }

    @RequestMapping(value = "/edit/save/{id}", method = RequestMethod.POST)
    public String saveEditCategory(@PathVariable("id") long id,@ModelAttribute("category") Category category) {

        Date date = new Date();
        category.setCreated_at(categoryService.get(id).getCreated_at());
        category.setUpdated_at(date);
        categoryService.save(category);
        return "redirect:/category/index";
    }

    @RequestMapping("/delete/{id}")
    public String deleteCategory(@PathVariable(name = "id") Long id) {
        categoryService.delete(id);
        return "redirect:/category/index";
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView showEditCategoryPage(@PathVariable(name = "id") int id) {
        ModelAndView mav = new ModelAndView("/category/edit");
        Category category = categoryService.get(id);
        mav.addObject("category", category);
        return mav;
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String search(@ModelAttribute("category") Category category) {
        Date date = new Date();
        category.setCreated_at(date);
        category.setUpdated_at(date);
        categoryService.save(category);

        return "redirect:/category/index";
    }
}
