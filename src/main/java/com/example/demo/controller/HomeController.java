package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller //  Specify HomeController is a controller
public class HomeController {
    // When user access to endpoint / then homepage() is called
    @GetMapping("/")
    public String homepage() {
        return "redirect:/product/index"; // Return index.html
    }
}
