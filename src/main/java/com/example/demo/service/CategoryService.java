package com.example.demo.service;

import com.example.demo.model.Category;
import com.example.demo.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
    
    @Autowired
    private CategoryRepository categoryRepo;

    public List<Category> listAll(String keyword) {
        if (keyword != null) {
            return categoryRepo.search(keyword);
        }
        return categoryRepo.findAll();
    }

    public List<Category> listAll() {

        return categoryRepo.findAll();
    }

    public void save(Category category) {
        categoryRepo.save(category);
    }

    public void saveAll(List<Category> category) {
        categoryRepo.saveAll(category);
    }

    public Category get(long id) {
        return categoryRepo.findById(id).get();
    }

    public void delete(long id) {
        categoryRepo.deleteById(id);
    }
}
