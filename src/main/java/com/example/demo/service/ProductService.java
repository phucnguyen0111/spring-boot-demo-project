package com.example.demo.service;

import com.example.demo.model.Product;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepo;

    public List<Product> listAll(String keyword) {
        if (keyword != null) {
            return productRepo.search(keyword);
        }
        return productRepo.findAll();
    }

    public List<Product> listAll() {

        return productRepo.findAll();
    }

    public void save(Product product) {
        productRepo.save(product);
    }

    public void saveAll(List<Product> product) {
        productRepo.saveAll(product);
    }

    public Product get(long id) {
        return productRepo.findById(id).get();
    }

    public void delete(long id) {
        productRepo.deleteById(id);
    }
}
