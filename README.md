# Spring boot CRUD demo

My first project using Spring boot 

## Configuration

- Create database name: demo_product_manage
- Modify file application.yml
    + To auto create table: ddl-auto: create
    + Change port from 3307 to your sql port (e.g. 3306)
- Access localhost:8081 to start

